﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WinFormsApp1
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x, b, h;
            int q = 0;

            if (double.TryParse(TextBox1.Text, out x))
            {
                if (double.TryParse(TextBox2.Text, out b))
                {
                    if (double.TryParse(TextBox3.Text, out h))
                    {
                        double v = (b - x) / h;
                        int z = (int)(Math.Ceiling(v));
                        double[] arr = new double[z];
                        double[] arr2 = new double[z];
                        do
                        {
                             
                            arr[q] = (Math.Pow(x, 3) - 2) / (3 * Math.Log(x));
                            arr2[q] = x;
                            q++;
                            x += h;
                        }
                        while (x < b);

                        string result = "x\ty\n";
                        for (int i = 0; i < arr.Length; i++)
                        {
                            string formattedNum1 = arr[i].ToString("F3"); 
                            string formattedNum2 = arr2[i].ToString("F3"); 

                            
                            result += $"{formattedNum2}\t{formattedNum1}";

                            
                            if (i < arr.Length - 1)
                            {
                                result += Environment.NewLine;
                            }
                        }

                       
                        TextBox4.Text = result;
                    }
                    else
                    {
                        MessageBox.Show("Помилка введення значення h!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Помилка введення значення b!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
