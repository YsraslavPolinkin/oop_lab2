﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double x, y, z;
           
            if (double.TryParse(TextBox1.Text, out x))
            {
                if (double.TryParse(TextBox2.Text, out y))
                {

                    if (double.TryParse(TextBox3.Text, out z))
                    {
                        if (x <= 0 || y <= 0 || z <= 0)
                        { Close(); }
                        else if (x + y > z || x + z > y || y + z > x) {MessageBox.Show("такий трикутник існує!", "", MessageBoxButton.OK); }

                        else
                        {
                            Close();
                        }
                    }

                    else 
                    {
                        MessageBox.Show("Помилка введення значення z!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    
                }
                else
                {
                    MessageBox.Show("Помилка введення значення y!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Помилка введення значення x!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
