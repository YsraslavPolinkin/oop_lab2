namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            textBox4.ReadOnly = true;
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double x, b, h;
            int q = 0;

            if (double.TryParse(textBox1.Text, out x))
            {
                if (double.TryParse(textBox2.Text, out b))
                {
                    if (double.TryParse(textBox3.Text, out h))
                    {
                        double v = (b - x) / h;
                        int z = (int)(Math.Ceiling(v));
                        double[] arr = new double[z];
                        double[] arr2 = new double[z];
                        do
                        {

                            arr[q] = (Math.Pow(x, 3) - 2) / (3 * Math.Log(x));
                            arr2[q] = x;
                            q++;
                            x += h;
                        }
                        while (x < b);

                        string result = "x\ty\n";
                        for (int i = 0; i < arr.Length; i++)
                        {
                            string formattedNum1 = arr[i].ToString("F3");
                            string formattedNum2 = arr2[i].ToString("F3");


                            result += $"{formattedNum2}\t{formattedNum1}";


                            if (i < arr.Length - 1)
                            {
                                result += Environment.NewLine;
                            }
                        }


                        textBox4.Text = result;
                    }
                    else
                    {
                        MessageBox.Show("������� �������� �������� h!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("������� �������� �������� b!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("������� �������� �������� a!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {

            double x, b, h;
            int q = 0;

            if (double.TryParse(textBox1.Text, out x))
            {
                if (double.TryParse(textBox2.Text, out b))
                {
                    if (double.TryParse(textBox3.Text, out h))
                    {
                        double v = (b - x) / h;
                        int z = (int)(Math.Ceiling(v));
                        double[] arr = new double[z];
                        double[] arr2 = new double[z];
                        do
                        {

                            arr[q] = (Math.Pow(x, 3) - 2) / (3 * Math.Log(x));
                            arr2[q] = x;
                            q++;
                            x += h;
                        }
                        while (x < b);

                        string result = "x\ty" + "\t";
                        for (int i = 0; i < arr.Length; i++)
                        {
                            string formattedNum1 = arr[i].ToString("F3");
                            string formattedNum2 = arr2[i].ToString("F3");


                            result += $"{formattedNum2}\t{formattedNum1}";


                            if (i < arr.Length - 1)
                            {
                                result += Environment.NewLine;
                            }
                        }


                        textBox4.Text = result;
                    }
                    else
                    {
                        MessageBox.Show("������� �������� �������� h!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("������� �������� �������� b!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("������� �������� �������� a!", "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}